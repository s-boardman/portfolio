import sys
import pendulum
import MySQLdb
import MySQLdb.cursors
import pandas as pd


def check_annotations_match_database(connection, sample, vars_for_db_import,
                                     alamut_db_version):
    """Assess whether the records in the database match the alamut annotations.

    Assess whether the records in the database match the alamut annotations.
    This is necessary because of the LRG assembly conversion and potential
    overlap of gNomen (since resolved by additional unique constraint).
    NB: Currently cannot check alamut unannotated variants.

    Args:
        connection: An instance of the database connection object.
        sample: An instance of a ProcessedSample object.
        vars_for_db_import (DataFrame): Pandas DataFrame object containing the
            variants for import of the given sample.
        alamut_db_version (str): Alamut Db Version date string;
            eg '2016-11-05'.

    """
    # Input DataFrame count
    alamut_total = 0
    # Database count
    db_total = 0
    with connection.cursor() as cur:
        try:
            # Read all annotations for all variants for a given sample into df
            values = (sample.lab_id, sample.batch.name, alamut_db_version)
            cur.callproc('sp_get_sample_annotated_variants', values)
            # Get all the rows returned by the stored procedure and convert to
            # a DataFrame
            data = cur.fetchall()
            db_df = pd.DataFrame.from_records(data)
            # Exract column names to use in df
        except MySQLdb.Error as e:
            handle_db_error(connection, cur, sample.seeding_log, e)

        # User a groupby to count the number of annotations (rows) for a given
        # set of genomic coordinates (representing a variant)
        for coord, annotations in vars_for_db_import.groupby(['chrom',
                                                              'gDNAstart',
                                                              'gDNAend']):
            # Equivalent to the number of rows; ie annotations per coordinate
            annotation_count = len(annotations.index)
            alamut_total += annotation_count
            # Find rows in the database records which represent annotations
            # related to the current variant coordinate
            db_rows = db_df[(db_df.chr == str(coord[0])) &
                            (db_df.start == int(coord[1])) &
                            (db_df.stop == int(coord[2]))]
            # Count the index of the subset dataframe to identify the number
            # of annotations
            db_count = len(db_rows.index)
            db_total += db_count
            # Identify coordinates with mismatched input and database
            # annotations and write them to the log file
            if db_count != annotation_count:
                messages = [('coord:{} alamut={}, db={}'
                             .format(coord, annotation_count, db_count)),
                            annotations, db_rows]
                [write_to_log(sample.seeding_log, m) for m in messages]
        m = ('{} alamut annotated variants checked against {} database'
             ' records.'.format(alamut_total, db_total))
        write_to_log(sample.seeding_log, m)


def db_connect(db_type):
    """Create a database connection using credentials stored in varDB_config.

    Using the MySQLdb package create a database connection to the given
    database.

    Args:
        db_type (str): Indicate if the 'test' or 'production' database should
            be used.

    Returns:
        connection: A MySQLdb connection instance.

    """
    if db_type == 'production':
        from varDB_config import production as db_details
    elif db_type == 'test':
        from varDB_config import test as db_details
    else:
        print('FATAL ERROR: INCORRECT DATABASE NAME PASSED.')
        sys.exit()
    print('Attempting to connect to database {} as user {}'.format(
          db_details['db'], db_details['user']))
    try:
        db_connection = MySQLdb.connect(host=db_details['host'],
                                        db=db_details['db'],
                                        user=db_details['user'],
                                        password=db_details['password'],
                                        cursorclass=MySQLdb.cursors.DictCursor)
        print('{}: Successfully connected to {} as {}'.format(
              pendulum.now().to_datetime_string(), db_details['db'],
              db_details['user']))
        return db_connection
    except MySQLdb.OperationalError as e:
        (print('FATAL: Connection error detected.\n{}\nAborting script.'
               .format(e)))
        sys.exit()


def db_close(connection):
    """Safely close the database connection."""
    print('{}: Closing connection to database.'.format(
          pendulum.now().to_datetime_string()))
    connection.close()


def handle_db_error(connection, cursor, log_file, error):
    """Standardised error handling for transactions in varDB.

    Print the error message

    Args:
        connection: Database connection object.
        cursor: Database cursor object.
        log_file: Filepath to the appropriate error log.
        error (str): Error message string.
    """
    m = (('\nError detected with transaction: {}\n{}'
         .format(cursor.description, error)))
    write_to_log(log_file, m)
    db_close(connection)
    sys.exit()


def write_to_log(log_file, message, verbose=True):
    """Write the message to the given log file.

    Put messages in log files. Also print them to the console by default.

    Args:
        log_file (str): Path to the log file.
        message (str): String to put in log file.
        verbose (bool): Print message to commandline. Defaults to True.

    """
    formatted_message = '{}: {}'.format(pendulum.now().to_datetime_string(),
                                        message)
    try:
        with open(log_file, 'a') as log:
            log.write(formatted_message + '\n')
        if verbose:
            print(formatted_message)
    except FileNotFoundError:
        print('FATAL ERROR: cannot open {} to input {}.'
              .format(log_file, message))
        sys.exit()
