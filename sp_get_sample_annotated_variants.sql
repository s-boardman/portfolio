/* get all variants present for a given sample,
with a given run date and given alamut_db_version.
*/

DROP PROCEDURE IF EXISTS sp_get_sample_annotated_variants;

DELIMITER //

CREATE PROCEDURE sp_get_sample_annotated_variants
(IN _lab_number text,
 IN _batch_name text,
 IN _alamut_db_version date)

BEGIN
    DECLARE alamut_db_id int;
    DECLARE samplebatch_id int;
    SELECT `alamut_db_version`.`alamut_db_version_id`
        FROM `alamut_db_version`
        WHERE `alamut_db_version` = _alamut_db_version INTO alamut_db_id;
    SELECT `sample_batch`.`id`
        FROM `sample_batch`
		INNER JOIN `batch` ON (`sample_batch`.`batch_id` = `batch`.`batch_id`)
		INNER JOIN `sample` ON (`sample_batch`.`sample_id` = `sample`.`sample_id`)
		WHERE (`batch`.`batch_name` = _batch_name AND `sample`.`lab_number` = _lab_number) INTO samplebatch_id;

 SELECT DISTINCT
  s.lab_number,
  sv.run_date,
  p.batch_name,
  p.panel_type_id,
  var.chr,
  var.start,
  var.stop,
  var.ref,
  var.alt,
  var.gnomen,
  var.assembly,
  sv.quality,
  sv.gt,
  sv.dp,
  sv.ad,
  g.gene_name,
  t.transcript_name,
  gen.exon,
  gen.intron,
  gen.cnomen,
  gen.pnomen,
  geno.rsid,
  geno.rsclinicalsignificance,
  geno.rsmaf,
  geno.thousand_g_af,
  geno.exac_af,
  geno.gnomad_af,
  geno.esp_af,
  a.alamut_db_version
FROM sample_batch_variant sv
		INNER JOIN sample_batch sb ON sv.samplebatch_id = sb.id
		INNER JOIN sample s ON s.sample_id = sb.sample_id
    INNER JOIN variant var ON var.variant_id = sv.variant_id
    INNER JOIN annotation anno ON var.variant_id = anno.variant_id
    INNER JOIN alamut_db_version a ON a.alamut_db_version_id = anno.alamut_db_version
    INNER JOIN genomic_annotation geno ON geno.g_anno_id = anno.genomic_annotation_id
    INNER JOIN genetic_annotation gen ON gen.anno_id = anno.genetic_annotation_id
    INNER JOIN gene g ON g.gene_id = gen.gene_id
    INNER JOIN transcript t ON t.transcript_id = gen.transcript_id
    INNER JOIN batch p ON p.batch_id AND p.batch_id = sb.batch_id
WHERE
	sv.samplebatch_id = samplebatch_id
        AND anno.alamut_db_version = alamut_db_id
ORDER BY
  var.assembly,
  var.chr,
  var.start,
  var.stop;

END; //

DELIMITER ;
