# A small Python and SQL showcase by sboardman

A collection of utility and processing functions for interacting with a
database. The main function in `check_variants.py` takes the details of a
sample and a DataFrame of the associated genomic variants and calls a stored
procedure (code found in `sp_get_sample_annotated_variants.sql`) to retrieve
all the variant records within the database associated with the input sample.

This process is used within the production script to sanity check that each
genomic coordinate has had the correct number of annotations input and linked
with it. To do this the rows in the input DataFrame are collapsed into unique
genomic coordinates and the number of associated annotations is compared to
the number of rows in the DataFrame returned from the stored procedure.
